# LibreMiami first online meetup, May 2 2020

## News
[Hospitals need to repair ventilators, manufacturers are making that impossible ](https://www.vice.com/en_us/article/wxekgx/hospitals-need-to-repair-ventilators-manufacturers-are-making-that-impossible)

## This meetup was library edition. 
Courtesy of LM:
* [archive.org national emergency library](https://archive.org/details/nationalemergencylibrary)
* [acm member library](https://dl.acm.org/)
* [LM blog with links to open textbooks and public domain books](https://lmemsm.dreamwidth.org/)
* [Compiler Design in C](https://holub.com/goodies/compiler/compilerDesignInC.pdf)

Courtesy of jgarte:
* [Against Continuity](https://edinburghuniversitypress.com/book-against-continuity.html)
* [Guix Cookbook](https://guix.gnu.org/cookbook/en/html_node/Basic-setup-with-manifests.html#Basic-setup-with-manifests)

Courtest of ?:
* [lvi attack](https://lviattack.eu/)

secret rare internet points have been recorded

## Business

### On the issue of obtaining non-profit status

Consensus: Yea

Todo list has been started in Wiki, more information needs to be gathered.

### Librebooting initiative

Waiting on male-to-female cables and Beagle Bone Black in mail. We will document the process and then be able to do a workshop. 

### On the issue of making libreboot computers for public schools

Consensus: Nay

A few members have experienced that schools are generally unreceptive of libre systems. 

However, there is interest in perhaps reaching out to the visually imparied. This is definitely a possibility. Initiative to begin post-pandemic

### On the issue of designing a macbook replacement in partnership with coding bootcamps

Consensus: Nay

Ultimately, the same resources are better spent on promoting and creating libre and gratis courses, books, tutorials, etc. 
and providing support for people self-educating. Public colleges and universities are recommendable beyond this.

### Continuing LibreMiami during COVID-19

* There will be an online meetup or other activity roughly every other week (first and third saturday 4-6pm)
* There is interest in master classes on librebooting, installing replicant and Linux From Scratch
* We can get guest speakers from anywhere in the world
* One of our only options for marketing is online, so all marketing efforts need to be in this direction
	* This makes finding a VPS usable without proprietary JavaScript urgent, issue will be created if none exists
	* Blogs will be posted weekly, website improved
		* Some have volunteered to write blogs
		* We can get other people to write guest blogs too
	* We need to reach out to more online communities
* We should continue having monthly online meetups post-pandemic. 

## Misc links shared by members

[Any ideas can be posted as issues](https://gitlab.com/libremiami/libremiami/-/issues)

[lvi](https://lviattack.eu/)

[brltty](https://www.systutorials.com/docs/linux/man/1-brltty/)

[create a searx instance issue](https://gitlab.com/libremiami/libremiami/-/issues/29)

[communication software fsf article](https://www.fsf.org/blogs/community/better-than-zoom-try-these-free-software-tools-for-staying-in-touch)

