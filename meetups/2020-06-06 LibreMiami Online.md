# LibreMiami Online June 06, 2020

## News

* https://www.engadget.com/zoom-explains-why-free-users-wont-get-encrypted-video-calls-103046648.html
* https://en.wikipedia.org/wiki/COINTELPRO

## Videos

* LM: https://www.blender.org/about/projects/
* Misha: https://archive.org/details/27794FireControlComputersPt1

## Business

### Device Drive

There may be local device drive initiatives happening already, contact Bill Hall

Possible Resources:

* https://www.freegeek.org/
* http://one.laptop.org/
* aftrr.org

### Guix Watch Party

Happening on Jitsi 6/20, being run by jgarte!

https://gettogether.community/events/5576/guix-talk-virtual-watch-party-extra-geeky/

### Next mid-month activity suggestions

We'll be doing some sort of programming laguage comparison watch party, help us plan it in the chat.

Possible Resources:

* http://pleac.sourceforge.net/

### Marketing

More blogs, videos in the works

https://github.com/snwh/computefreely

### Hosting a service on the VPS

We want to 'hello world' the deployment/hosting process so we ideally want something

1) Simple to install
2) Useful
3) Customizable so we can add LibreMiami branding

Ideas:

* Searx Instance (+1)
* Simple Git Repo
* File Sharing

Resources:

* https://libreprojects.net/
* https://medium.com/@robaboukhalil/porting-games-to-the-web-with-webassembly-70d598e1a3ec
* https://github.com/humu2009/tinygl.js?files=1


Possible resources

### Libreboot

Librebooting an x200, @ roberto or jgarte in the chat if you want to lurk



## Misc Links

* https://k1ss.org/
* https://github.com/froggey/Mezzano/
* https://mediagoblin.org/


