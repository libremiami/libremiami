# LibreMiami

This is the meta project for [LibreMiami](https://libremiami.org) to track issues and files for the group itself.

## How to use

If there is some improvement you want to make to LibreMiami itself (not a particular project), you can create a new issue.
If you have ideas about an existing issue, you can comment on it to start a discussion. If you want to help out with something you can 
assign an issue to yourself. 

## Directories

You can commit files for the group to this repo (logos, marketing materials, meeting notes etc.). 

## Logo License

For now, the LibreMiami logos are Copyright 2019-2020 Roberto Beltran, all rights reserved, you have revocable permission to use or modify the logos as a LibreMiami member for related projects and to promote the group.
